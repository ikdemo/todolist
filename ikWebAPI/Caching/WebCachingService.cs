﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ikWebAPI.Caching
{
    internal class WebCachingService
    {
        //double _cachedTimeOut = Convert.ToDouble(45);

        #region ..ctor
        public WebCachingService()
        {

        }
        #endregion

        #region PUBLIC METHOD
        public object GetCache(string key)
        {
            if (HttpContext.Current == null || HttpContext.Current.Cache[key] == null)
            {
                return null;
            }
            else
            {
                object dsData = HttpContext.Current.Cache[key];
                return dsData;
            }
        }
        public DataSet GetDataSet(string key)
        {
            object value = null;
            if (HttpContext.Current != null && HttpContext.Current.Cache[key] != null)
            {
                value = HttpContext.Current.Cache[key];
            }
            return (DataSet)value;
        }
        public void SetDataset(string key, object value)
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Cache.Insert(key, value, null,
                           System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(45));
            }
        }
        public bool TryGet(string key, out object value)
        {
            value = null;
            if (HttpContext.Current != null && HttpContext.Current.Cache[key] != null)
            {
                value = HttpContext.Current.Cache[key];
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetCache(string key, object value)
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Cache.Insert(key, value, null,
                           System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(45));
            }
        }

        public void RemoveCache(string key)
        {
            if (HttpContext.Current != null)
            {
                if (GetCache(key) != null)
                {
                    HttpContext.Current.Cache.Remove(key);
                }
            }
        }

        public void FlushAllCache()
        {
            //throw new NotImplementedException();
        }
        public T GetTypedCache<T>(string key)
        {
            object value = null;
            if (HttpContext.Current != null && HttpContext.Current.Cache[key] != null)
            {
                value = HttpContext.Current.Cache[key];
            }
            return (T)value;
        }
        public void SetTypedCache<T>(string key, T value)
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Cache.Insert(key, value, null,
                           System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(45));
            }
        }
        #endregion   
    }
}