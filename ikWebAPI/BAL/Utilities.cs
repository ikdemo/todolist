﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ikWebAPI.BAL
{
    public class Utilities
    {
        public enum ViewType
        {
            Current = 1,
            Past = 2
        }

        public static bool ConvertToBool(string value)
        {
            bool returnVal = false;
            if (!string.IsNullOrWhiteSpace(value))
            {
                switch (value.ToLower())
                {
                    case "true":
                    case "1":
                        returnVal = true;
                        break;
                }
            }

            return returnVal;
        }

        public static DateTime ConvertToDateTime(string value)
        {
            DateTime returnVal = DateTime.MinValue;
            try
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    returnVal = Convert.ToDateTime(value);
                }
            }
            catch { }

            return returnVal;
        }
    }
}