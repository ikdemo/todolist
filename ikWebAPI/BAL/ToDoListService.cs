﻿using ikWebAPI.Caching;
using ikWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using static ikWebAPI.BAL.Utilities;

namespace ikWebAPI.BAL
{
    public class ToDoListService
    {
        private WebCachingService _cachingService;

        public ToDoListService()
        {
            _cachingService = new WebCachingService();
        }

        public DataTable GetValues()
        {
            DataTable dtValues = null;
            object objTemp = _cachingService.GetCache("ToDoList");
            if (objTemp != null && objTemp is DataTable dt)
            {
                dtValues = dt;
            }
            else
            {
                dtValues = getTempData();
            }

            return dtValues;
        }

        public DataTable GetValueById(int id)
        {
            DataTable dtSingleValue = null;

            DataTable dtValues = GetValues();
            if (dtValues != null && dtValues.Rows.Count > 0)
            {
                DataRow[] drSel = dtValues.Select("Id = " + id);
                if (drSel.Length > 0)
                {
                    dtSingleValue = drSel.CopyToDataTable();
                }
            }

            return dtSingleValue;
        }

        public DataTable GetValueByViewType(int viewType)
        {
            DataTable dtSingleValue = null;

            DataTable dtValues = GetValues();
            if (dtValues != null && dtValues.Rows.Count > 0)
            {
                string filter = string.Empty;
                string sort = string.Empty;

                switch (viewType)
                {
                    case (int)ViewType.Past:
                        filter = "IsCompleted = " + true + " or IsDeleted = " + true;
                        sort = "UpdatedDttm desc";
                        break;
                    default:
                        filter = "IsCompleted = " + false + " and IsDeleted = " + false;
                        sort = "CreatedDttm desc";
                        break;
                }

                DataRow[] drSel = dtValues.Select(filter, sort);
                if (drSel.Length > 0)
                {
                    dtSingleValue = drSel.CopyToDataTable();
                }
            }

            return dtSingleValue;
        }

        public bool AddNewValue(ToDoListItem value)
        {
            bool isAdded = false;
            DataTable dtValues = GetValues();
            if (dtValues != null)
            {
                int maxId = 0;
                DateTime currentTime = DateTime.Now;

                if (dtValues.Rows.Count > 0)
                {
                    maxId = Convert.ToInt32(dtValues.Compute("max(Id)", ""));
                }

                DataRow drNew = dtValues.NewRow();
                drNew["Id"] = (maxId + 1);
                drNew["Description"] = value.Description;
                drNew["IsCompleted"] = false;
                drNew["IsDeleted"] = false;
                drNew["CreatedDttm"] = currentTime;
                drNew["UpdatedDttm"] = currentTime;
                dtValues.Rows.Add(drNew);

                _cachingService.SetCache("ToDoList", dtValues);

                isAdded = true;
            }

            return isAdded;
        }

        public bool UpdateValue(int id, ToDoListItem value)
        {
            bool isUpdated = false;
            DataTable dtValues = GetValues();
            if (dtValues != null && dtValues.Rows.Count > 0)
            {
                if (dtValues.Select("Id = " + id).Any())
                {
                    DataRow[] drSel = dtValues.Select("Id = " + id + " and IsCompleted = " + false + " and IsDeleted = " + false);
                    if (drSel.Length > 0)
                    {
                        drSel[0]["IsCompleted"] = value.IsCompleted;
                        drSel[0]["IsDeleted"] = value.IsDeleted;
                        drSel[0]["UpdatedDttm"] = DateTime.Now;
                        drSel[0]["UpdatedDttm"] = DateTime.Now;
                        drSel[0]["StatusText"] = (value.IsDeleted) ? "Deleted" : "Completed";

                        _cachingService.SetCache("ToDoList", dtValues);
                    }

                    isUpdated = true;
                }
            }

            return isUpdated;
        }

        public bool DeleteValue(int id)
        {
            bool isDeleted = false;
            DataTable dtValues = GetValues();
            if (dtValues != null && dtValues.Rows.Count > 0)
            {
                if (dtValues.Select("Id = " + id).Any())
                {
                    DataRow[] drSel = dtValues.Select("Id <> " + id);
                    if (drSel.Length > 0)
                    {
                        _cachingService.SetCache("Values", drSel.CopyToDataTable());
                    }
                    else
                    {
                        _cachingService.SetCache("Values", dtValues.Clone());
                    }

                    isDeleted = true;
                }
            }

            return isDeleted;
        }

        private DataTable getTempData()
        {
            DataTable dtTemp = new DataTable();
            dtTemp.Columns.Add("Id", typeof(int));
            dtTemp.Columns.Add("Description", typeof(string));
            dtTemp.Columns.Add("IsCompleted", typeof(bool));
            dtTemp.Columns.Add("IsDeleted", typeof(bool));
            dtTemp.Columns.Add("StatusText", typeof(string));
            dtTemp.Columns.Add("CreatedDttm", typeof(DateTime));
            dtTemp.Columns.Add("UpdatedDttm", typeof(DateTime));

            dtTemp.Rows.Add(1, "Subscribe iKarumpalagai", false, false, "", DateTime.Now, DateTime.Now);
            dtTemp.Rows.Add(2, "Like This Video", false, false, "", DateTime.Now, DateTime.Now);
            dtTemp.Rows.Add(3, "Share This Video Link to your Friends", false, false, "", DateTime.Now, DateTime.Now);
            dtTemp.Rows.Add(4, "Follow us on Instagram", false, false, "", DateTime.Now, DateTime.Now);

            _cachingService.SetCache("ToDoList", dtTemp);

            return dtTemp;
        }
    }
}