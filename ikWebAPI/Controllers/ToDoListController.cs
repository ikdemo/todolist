﻿using ikWebAPI.BAL;
using ikWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ikWebAPI.Controllers
{
    public class ToDoListController : ApiController
    {
        private ToDoListService _toDoListService = new ToDoListService();

        // GET api/<controller>/1
        public List<ToDoListItem> Get(int viewType)
        {
            List<ToDoListItem> response = null;

            DataTable dtValue = _toDoListService.GetValueByViewType(viewType);
            if (dtValue != null && dtValue.Rows.Count > 0)
            {
                response = new List<ToDoListItem>();
                ToDoListItem itemVal = null;
                for (int itemIndex = 0; itemIndex < dtValue.Rows.Count; itemIndex++)
                {
                    itemVal = new ToDoListItem();
                    itemVal.Id = Convert.ToInt32(dtValue.Rows[itemIndex]["Id"]);
                    itemVal.Description = dtValue.Rows[itemIndex]["Description"].ToString();
                    itemVal.StatusText = dtValue.Rows[itemIndex]["StatusText"].ToString();
                    itemVal.IsCompleted = Utilities.ConvertToBool(dtValue.Rows[itemIndex]["IsCompleted"].ToString());
                    itemVal.IsDeleted = Utilities.ConvertToBool(dtValue.Rows[itemIndex]["IsDeleted"].ToString());
                    itemVal.CreatedDateTime = Utilities.ConvertToDateTime(dtValue.Rows[itemIndex]["CreatedDttm"].ToString()).ToString("dd MMM yyyy hh:mm tt");
                    itemVal.UpdatedDateTime = Utilities.ConvertToDateTime(dtValue.Rows[itemIndex]["UpdatedDttm"].ToString()).ToString("dd MMM yyyy hh:mm tt");

                    response.Add(itemVal);
                }
            }

            return response;
        }

        // POST api/<controller>
        public bool Post([FromBody]ToDoListItem value)
        {
            bool isAdded = _toDoListService.AddNewValue(value);
            return isAdded;
        }

        // PUT api/<controller>
        public bool Put(int id, [FromBody]ToDoListItem value)
        {
            bool isUpdated = _toDoListService.UpdateValue(id, value);
            return isUpdated;
        }
    }
}
