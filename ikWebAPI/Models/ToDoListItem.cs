﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ikWebAPI.Models
{
    public class ToDoListItem
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsDeleted { get; set; }
        public string StatusText { get; set; }
        public string CreatedDateTime { get; set; }
        public string UpdatedDateTime { get; set; }    
    }
}